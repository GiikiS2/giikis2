import { atom } from 'nanostores';
import axios from "axios";


export const lanyardata = atom<any>([]);

export const codestats = atom([]);

export const joke = atom("");

export function fetchSetLanyard() {
  axios.get("https://api.lanyard.rest/v1/users/240269142848962560").then((res) => {
    lanyardata.set(res.data.data)
  })
}

fetchSetLanyard()

export interface CodeStatsRes {
  languages: {
    [key: string]: { langClass: string; xps: number };
  };
}

const denylangs = [
  "JSON", "Log", "Plain text", "scminput", "Properties",
  "dockercompose", "Git", "Markdown", "TOML", "Ignore"
];

const langMapping: Record<string, string> = {
  CSS: "css3",
  HTML: "html5",
  "TypeScript (JSX)": "typescript",
  "JavaScript (JSX)": "javascript",
};

export const LangCol: Record<string, string> = {
  CSS: "sapphire",
  HTML: "peach",
  "TypeScript (JSX)": "blue",
  "TypeScript": "blue",
  "JavaScript (JSX)": "yellow",
  "JavaScript": "yellow",
  "PowerShell": "blue",
  "tailwindcss": "sky",
  "PostCSS": "red",
  "astro": "maroon",
  "Python": "yellow",
  "YAML": "red",
  "Go": "sky",
  "zig": "peach",
  "GLSL": "blue",
  "Java": "peach",
  "Shell Script": "green"
};

export function fetchSetCDs() {
  axios.get("https://codestats.net/api/users/giikis2").then((res) => {
    var codeStatsData: CodeStatsRes = res.data;

    const filteredLangs = Object.entries(codeStatsData.languages)
      .filter(([language]) => !denylangs.includes(language))
      .sort((a, b) => b[1].xps - a[1].xps)
      .map(([language, { xps }]) => [
        language,
        {
          langClass: langMapping[language] || language.toLowerCase(),
          xps,
        },
      ]);

    codestats.set(filteredLangs);
  });

}

fetchSetCDs()


export function fetchSetCat() {
  axios.get("https://catfact.ninja/fact").then((res) => {
    joke.set(res.data.fact);
  })
}

fetchSetCat()

