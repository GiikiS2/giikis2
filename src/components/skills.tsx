import { createSignal, onMount } from "solid-js";
import { useStore } from '@nanostores/solid';
import { Icon } from "@iconify-icon/solid";
import axios from "axios";
import { codestats, fetchSetCDs, LangCol } from "../data";

const LEVEL_FACTOR = 0.025;

function get_level(xp: number) {
  return Math.floor(LEVEL_FACTOR * Math.sqrt(xp));
}

function get_next_level_xp(level: number) {
  return Math.pow(Math.ceil((level + 1) / LEVEL_FACTOR), 2);
}


function Skills() {
  const $codestats = useStore(codestats);


  onMount(() => {
    fetchSetCDs()
  });

  return (
    <>

      {/* ignore, just to generate classes */}
      <span class="text-maroon">
        <span class="text-blue"></span>
        <span class="text-peach"></span>
        <span class="text-sapphire"></span>
        <span class="text-sky"></span>
      </span>
      
      <section>
        <h2>My skills:</h2>
        <h2 class="text-left">
          <a href="https://codestats.net/users/giikis2" class="flex gap-2 items-center">
            {/* <img src="/assets/codestats.png" class="w-5"></img>Code::Stats:</a> */}
            <span class="icon-cs"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span></span>
            Code::Stats:</a>
        </h2>
        <div class="flex justify-around m-2 rounded-lg flex-wrap w-full">
          {
            $codestats().length <= 0 ? (<> omg wheres data? </>) :
              $codestats().map(([language, { langClass, xps }]: any) => (
                <div class="w-4/12 m-1 p-1 px-2 text-lg hover:text-pink border border-transparent hover:border-green rounded-lg flex flex-col">
                  <span class="flex justify-center gap-1 items-center">
                    {language === "GLSL" ? (
                      // <Icon icon="vscode-icons:file-type-glsl" style="color: #a1a1a7;" />
                      // <img class="w-5" src="/assets/glsl.png"></img>
                      <span class="icon-glsl"><span class="path1"></span><span class="path2"></span></span>
                    ) : language === "Shell Script" ? (
                      <Icon icon="vscode-icons:file-type-script" />
                    ) : language === "Rust" ? (<img class="w-5" src="/assets/ferris_smile.png"></img>) : (
                      <i class={`devicon-${langClass}-plain text-${LangCol[language] || language.toLowerCase()}`} />
                    )}
                    {language}
                  </span>
                  <span class="flex justify-center gap-2 items-center">
                    <span>level {get_level(xps)}</span>
                    <progress
                      class="w-3/6 h-2/6 rounded-lg bg-[#00000000] [&::-webkit-progress-value]:bg-green [&::-moz-progress-bar]:bg-green border-green border"
                      value={xps}
                      max={get_next_level_xp(get_level(xps))}
                    />
                    <span>{get_level(get_next_level_xp(get_level(xps)))}</span>
                  </span>
                </div>
              ))}
        </div>
      </section>
    </>
  );
}

export default Skills;
