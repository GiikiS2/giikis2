import { createSignal, onMount } from "solid-js";
import { fetchSetLanyard, lanyardata } from "../data";
import gsap from "gsap";
import { useStore } from "@nanostores/solid";

function Lanyard() {
  const $lanyardata = useStore(lanyardata);

  onMount(() => {
    fetchSetLanyard();
  });

  const marqueeanim = (marquee) => {
    if (!marquee) return;
    const marqueeInner = marquee.querySelector('.marquee__inner');
    const marqueeContent = marqueeInner.querySelector('.marquee__content');

    const duration = marquee.getAttribute('data-marquee-duration');
    const marqueeContentClone = marqueeContent.cloneNode(true);
    marqueeInner.append(marqueeContentClone);

    const marqueeContentAll = marqueeInner.querySelectorAll('.marquee__content');

    marqueeContentAll.forEach((element) => {
      gsap.to(element, {
        x: "-101%",
        repeat: -1,
        duration: duration,
        ease: 'linear',
      });
    });
  };

  return (
    <div class="w-full text-center border border-green rounded-lg overflow-hidden flex flex-col justify-center items-center" id="discord">
      <h1 class="h-full w-full p-1 bubble thought flex items-center justify-center">
        {($lanyardata() && $lanyardata().activities && $lanyardata().activities.length > 0) && (
          <section>
            {//@ts-ignore
              <div class="marquee !transition-none !duration-0" use:marqueeanim data-marquee-duration="10">
              <div class="marquee__inner !transition-none !duration-0">
                <div class="marquee__content text-nowrap !transition-none !duration-0">
                  {$lanyardata().activities[0].emoji.name} {$lanyardata().activities[0].state}
                </div>
              </div>
            </div>}
          </section>
        )}
      </h1>

      {$lanyardata()?.discord_user?.avatar && <img class="pfp my-1" src={`https://cdn.discordapp.com/avatars/240269142848962560/${$lanyardata()?.discord_user?.avatar}`} />}

      <h1 class="border-green w-full border-t border-1 p-1">
        {$lanyardata().discord_status && (
          <p>
            Me{" "}
            <span
              class={
                $lanyardata().discord_status === "idle"
                  ? "text-yellow moon-after"
                  : $lanyardata().discord_status === "online"
                    ? "text-green flower-after"
                    : $lanyardata().discord_status === "dnd"
                      ? "text-red straw-after"
                      : $lanyardata().discord_status === "offline"
                        ? "text-grey ghost-after"
                        : ""
              }
            >
              {$lanyardata().discord_status}
            </span>
          </p>
        )}
      </h1>
    </div>
  );
}

export default Lanyard;
