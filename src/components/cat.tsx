import { createSignal, onMount } from "solid-js";
import { useStore } from '@nanostores/solid';
import { Icon } from "@iconify-icon/solid";
import { fetchSetCat, joke } from "../data";

function Cat() {
  const $joke = useStore(joke);

  onMount(() => {
    fetchSetCat()
  });

  return (
    <>
    {$joke()}
    </>
  );
}

export default Cat;
