import { createSignal, onMount } from "solid-js";
import { fetchSetLanyard, lanyardata } from "../data";
import gsap from "gsap";
import { useStore } from "@nanostores/solid";

import { horizontalLoop } from "../lib/hl";

function Lanyard() {
  const $lanyardata = useStore(lanyardata);

  const marqueeanim = (marquee) => {
    const marqueeInner = marquee.querySelector('.marquee__inner');
    const marqueeContent = marqueeInner.querySelector('.marquee__content');

    const marqueeContentClone = marqueeContent.cloneNode(true);
    marqueeInner.append(marqueeContentClone);
    const marqueeContentClone2 = marqueeContent.cloneNode(true);
    marqueeContentClone2.classList.add("pr-4")
    marqueeInner.append(marqueeContentClone2);

    const boxes = gsap.utils.toArray(".marquee__content")

    const loop = horizontalLoop(boxes, { paused: false, repeat: -1, speed: .25, center: false });
  };

  onMount(() => {
    fetchSetLanyard();
  });

  return (
    <div class="w-full text-center border border-green rounded-lg overflow-hidden flex flex-col justify-center items-center" id="discord">
      <h1 class="h-full w-full p-1 bubble thought flex items-center justify-center">

        <section>
          {//@ts-ignore
            <div class="!transition-none !duration-0" use:marqueeanim data-marquee-duration="10">
              <div class="marquee__inner flex items-start flex-row !justify-start gap-4 !transition-none !duration-0">
                <div class="marquee__content text-clip text-nowrap text-left !transition-none !duration-0">
                  {$lanyardata()?.activities?.[0].emoji.name} {$lanyardata().activities?.[0].state}
                </div>
              </div>
            </div>}
        </section>

      </h1>

      {<img class="pfp my-1" src={`https://cdn.discordapp.com/avatars/240269142848962560/${$lanyardata().discord_user?.avatar}`} />}

      <h1 class="border-green w-full border-t border-1 p-1">
        <p>
          Me{" "}
          <span
            class={
              $lanyardata().discord_status === "idle"
                ? "text-yellow moon-after"
                : $lanyardata().discord_status === "online"
                  ? "text-green flower-after"
                  : $lanyardata().discord_status === "dnd"
                    ? "text-red straw-after"
                    : $lanyardata().discord_status === "offline"
                      ? "text-grey ghost-after"
                      : ""
            }
          >
            {$lanyardata().discord_status}
          </span>
        </p>
      </h1>
    </div>
  );
}

export default Lanyard;
