import { createSignal, createEffect, createMemo } from "solid-js";
import { Icon } from "@iconify-icon/solid";
import Accordion from '@corvu/accordion';
import Codeblock from "./codeblock";

const imgClass = "p-1 m-2 rounded-lg";

function Home() {
    return (
        <>
            <section id="about">
                <h1 class="h1 w-full">
                    Welcome to my webpage! <span class="text-base">👋👋👋👋</span>
                </h1>
                <div class="flex justify-between h-28 m-2 border border-green rounded-lg overflow-auto">
                    {[
                        ["kirby gif", "kirby.gif"],
                        ["floppa family :3", "floppas.png"],
                        ["raccon with guitar :0", "racute.jpg"]
                    ].map((img) => (
                        <img alt={img[0]} class={imgClass} src={"/assets/" + img[1]} data-tippy-content={img[0]} />
                    ))}
                </div>
                <p class="bg-black p-4 rounded-lg bg-opacity-15">
                    My name = <strong class="font-bold bg-clip-text text-text hover:text-transparent hover:bg-[linear-gradient(to_right,#f38ba8,#fab387,#89b4fa,#cba6f7,theme(colors.purple.400))] bg-[length:200%_auto] animate-gradient">Giiki_s2</strong> and I use any pronoums. :) I like playing
                    with friends, programming and doing crafts. {">"}:)
                </p>
            </section>

            <section class="mt-4">
                <h2>My social media:</h2>
                <div class="flex justify-around m-2 border border-green rounded-lg overflow-hidden">
                    {[
                        "Neocities>https://neocities.org/site/giikis2",
                        "Discord>https://discord.com/users/240269142848962560",
                        "Gitlab>https://gitlab.com/GiikiS2",
                        "Twitter>https://twitter.com/giikis2",
                        "Youtube>https://www.youtube.com/@giiki_s2",
                    ].map((button) => (
                        <a href={button.split(">")[1]}>
                            <button
                                data-tippy-content={button.split(">")[0]}
                                class="m-1 p-1 hover:text-pink border border-transparent hover:border-green rounded-lg"
                            >
                                {button.split(">")[0] === "Neocities" ?
                                    <span class="icon-neocities"><span class="path1"></span><span class="path2"></span></span> :
                                    <i class={`fa-brands fa-${button.split(">")[0].toLowerCase()}`} />}
                            </button>
                        </a>
                    ))}
                </div>
            </section>


            <section class="p-2 w-full">
                {/* <Codeblock /> */}

                <div class="!flex flex-col w-full rounded-lg overflow-hidden bg-black bg-opacity-15 flex-wrap text-wrap">
                    <Accordion collapseBehavior="hide">
                        <Accordion.Item>
                            <h2>
                                <Accordion.Trigger class="flex gap-2 justify-start items-center">
                                    <div>
                                        <Icon icon="solar:square-alt-arrow-right-bold-duotone" class="icon text-lg" inline />
                                    </div>
                                    <p class="w-full text-center">Wanna include me on ur website? ( ´ ▽ ` )<br /> heres my badge!</p>
                                </Accordion.Trigger>
                            </h2>
                            <Accordion.Content class="flex justify-center flex-wrap text-wrap w-full" style="--corvu-disclosure-content-width: 100%">
                                <Codeblock />
                            </Accordion.Content>
                        </Accordion.Item>
                    </Accordion>
                </div>
            </section>
        </>
    );
}

export default Home;
