import { createSignal, createEffect } from "solid-js";
import Prism from "prismjs";
import "prismjs/plugins/line-numbers/prism-line-numbers.css"; // Import the line-numbers plugin CSS
import "prism-themes/themes/prism-shades-of-purple.css"; // Import the chosen theme

// Include the line-numbers plugin
import "prismjs/plugins/line-numbers/prism-line-numbers";

function Codeblock() {
  const codeString = 
  `<a href="https://giikis2.neocities.org/">
  <img src="https://giikis2.neocities.org/assets/badges/giikis2.png" width="88" height="31" />
</a>`;

  const [copied, setCopied] = createSignal(false);

  const copyToClipboard = () => {
    navigator.clipboard.writeText(codeString)
      .then(() => {
        console.log('Code copied to clipboard');
        setCopied(true); // Set copied state to true
        setTimeout(() => setCopied(false), 3000); // Reset copied state after 3 seconds
      })
      .catch(err => {
        console.error('Failed to copy: ', err);
      });
  };

  createEffect(() => {
    Prism.highlightAll(); // This will highlight all code blocks
  });

  return (
    <div class="flex w-full m-2 mx-1">
      <div class="bg-[#181825] text-white p-3 rounded-md w-full">
        <div class="flex justify-between items-center mb-2">
          <span class="text-gray-400">Code:</span>
          <button
            class="px-3 py-1 rounded-md flex flex-nowrap gap-1 items-center"
            onClick={copyToClipboard}
          >
            <img src="/assets/racute.jpg" class={"h-[1.1rem] transition-all duration-300 " + (copied() ? "w-4" : "w-0")} alt="Racute" />
            Copy
          </button>
        </div>
        <div class="overflow-x-auto max-w-full">
          <pre id="code" class="text-gray-300 !bg-base !py-2 !px-12 !m-0 rounded line-numbers">
            <code class="language-js">
              {codeString}
            </code>
          </pre>
        </div>
      </div>
    </div>
  );
}

export default Codeblock;
