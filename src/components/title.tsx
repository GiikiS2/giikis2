import { createSignal, createEffect, createMemo, onMount } from "solid-js";
import { Icon } from "@iconify-icon/solid";
import gsap from "gsap";

function Title() {
    var factor = 1.2;
    var speedFactor = 1.23

    const [isAnimated, setIsAnimated] = createSignal(false);

    //@ts-ignore
    const hoverAnimation = (logo) => {
        var gelatineAnimation = gsap.timeline({
            paused: true,
            repeat: -1,
        })
            .to(logo, { scaleX: 0.9 * factor, scaleY: 1.1 * factor, duration: 0.125 * speedFactor })
            .to(logo, { scaleX: 1.1 * factor, scaleY: 0.9 * factor, duration: 0.125 * speedFactor  })
            .to(logo, { scaleX: 0.95 * factor, scaleY: 1.05 * factor, duration: 0.125 * speedFactor  })
            .to(logo, { scaleX: 1 * factor, scaleY: 1 * factor, duration: 0.125 * speedFactor  });

        logo.addEventListener("mouseenter", () => {
            setIsAnimated(true)
            gsap.to(logo, { scaleX: 1 * factor, scaleY: 1 * factor, duration: 0.125 * speedFactor  })
                .then(() => isAnimated() && gelatineAnimation.restart());
        });

        logo.addEventListener("mouseleave", () => {
            setIsAnimated(false)

            gelatineAnimation.pause();
            gsap.to(logo, { scaleX: 1, scaleY: 1, duration: 0.225 * speedFactor  });
        });
    };

    return (
        <h1 class="p-3 pt-0 text-4xl starborn text-white text-center flex w-full justify-center gap-2">
            <a href="/" class="woah duration-0 px-1 logo"
                //@ts-ignore
                use:hoverAnimation>Giiki´s Lair</a>

            <button><Icon icon="akar-icons:language" /></button>
        </h1>
    );
}

export default Title;
