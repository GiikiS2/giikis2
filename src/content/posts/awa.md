---
title: 'Furby´s bath'
icon: "fa-bath"
description: 'Lily and Milo´s bath day :0'
date: '2022-07-11T20:51:59.705Z'
---
Friday I started removing Milo´s (turtle furby) skin,
![Removing Milo´s skin](https://64.media.tumblr.com/6a19b65985ec8bcbfcdcce5afcb30abb/fabd50e36eb5df02-25/s400x600/14027938a3f3b6911741bfb549fe7b31c506732a.jpg)

and Saturday with the skin in hands, I washed them.
![Washing the furbies](https://64.media.tumblr.com/3068ac45dbb8ca1322d4d7a09fe35656/fabd50e36eb5df02-24/s400x600/808693e29b49987df9154506f059825ac642a06d.jpg)

I let them dry on the rest of the day.

So sunday I put their skins back >)
![Putting the skin on the furbies back](https://64.media.tumblr.com/b1ad05f54c577ed76eaacf6af32cf427/fabd50e36eb5df02-e2/s1280x1920/5576bc444ea3aabe116917ae933f5ead5770d75c.jpg)

and now their clean and happy :)
![Furbies on the bed :)](https://64.media.tumblr.com/465983cd238aa2074c1d1f8fe2687a17/fabd50e36eb5df02-8d/s400x600/dbba9f20a0eb2f19791594d3e00045ab4889ec0e.png)
