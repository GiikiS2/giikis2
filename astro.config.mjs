import { defineConfig } from 'astro/config';
import tailwind from "@astrojs/tailwind";
import solidJs from "@astrojs/solid-js";

// import vercel from "@astrojs/vercel/serverless";
import Icons from 'unplugin-icons/vite'
// https://astro.build/config
export default defineConfig({
  // output: "hybrid",
  integrations: [tailwind(), solidJs()],
  // adapter: vercel(),
  vite: {
    plugins: [
        Icons({
            compiler: 'solid'
        })
    ]
}

});