import AOS from "aos";

import "aos/dist/aos.css";

function pop(e: any) {
  for (let i = 0; i < 5; i++) {
    createParticle(e.clientX, e.clientY);
  }
}

function createParticle(x: any, y: any) {
  const particle = document.createElement("particle");
  document.body.appendChild(particle);

  // Calculate a random size from 5px to 25px
  const size = Math.floor(Math.random() * 20 + 10);
  particle.style.width = `${size}px`;
  particle.style.height = `${size}px`;

  particle.style.backgroundImage = "url('/star_bounce.gif')";

  // Generate a random x & y destination within a distance of 75px from the mouse
  const destinationX = x + (Math.random() - 0.5) * 2 * 55;
  const destinationY = y + (Math.round(Math.random()) * 2 - 1 - 0.5) * 2 * 35;

  // Store the animation in a variable as we will need it later
  const animation = particle.animate(
    [
      {
        transform: `translate(-50%, -50%) translate(${x}px, ${y}px)`,
        opacity: 1,
      },
      {
        transform: `translate(${destinationX}px, ${destinationY}px)`,
        opacity: 0,
      },
    ],
    {
      // Set a random duration from 500 to 1500ms
      duration: Math.random() * 6000 + 500,
      easing: "cubic-bezier(0, .9, .57, 1)",
      // Delay every particle with a random value of 200ms
      delay: Math.random() * 150,
    }
  );

  // When the animation is complete, remove the element from the DOM
  animation.onfinish = () => {
    particle.remove();
  };
}

document.onclick = function (event) {
  //if (event === undefined) event = window.event;
  pop(event);
};

AOS.init({
  offset: 200,
  duration: 1890,
  easing: "ease-in-out-cubic",
  delay: 20,
});
