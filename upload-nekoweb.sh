#!/bin/bash

# Colors for logging
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
BLUE='\033[0;34m'
# shellcheck disable=SC2034
MAGENTA='\033[0;35m'
# shellcheck disable=SC2034
GRAY='\033[0;30m'
# shellcheck disable=SC2034
CYAN='\033[0;36m'
NC='\033[0m' # No Color

# API details
if [ -z "$API_KEY" ]; then
    echo "$RED Error: API_KEY environment variable is not set."
    exit 1
fi
UPLOAD_PATH="$1" # Path to upload
TARGET_DIR="./"  # Target directory on the server

# Function to handle errors
handle_error() {
    echo -e "${RED}Error occurred during upload.${NC}"
    exit 1
}

ask_question() {
    local question="$1"
    local default_answer="$2"
    local timeout=30

    echo "$question"
    read -t "$timeout" -r user_answer

    if [ $? -eq 0 ]; then
        echo "You answered: $user_answer"
    else
        echo "No answer received within $timeout seconds. Using default answer: $default_answer"
        user_answer="$default_answer"
    fi

    echo "$user_answer"
}

# Check if file or directory
# if [[ -d "$UPLOAD_PATH" ]]; then
#     echo -e "${YELLOW}Reading directory...${NC}"
#     FILES=$(find "$UPLOAD_PATH" -type f)
# elif [[ -f "$UPLOAD_PATH" ]]; then
#     FILES="$UPLOAD_PATH"
# else
#     echo -e "${RED}Invalid path specified.${NC}"
#     exit 1
# fi


    FILES=$(find dist/ -type f -name '[!.]*')

# Loop through files and upload
for FILE in $FILES; do
    FILE_SIZE=$(stat -c%s "$FILE")

    if [[ $FILE_SIZE -gt 100000000 ]]; then
        echo -e "${YELLOW} large file: $FILE${NC}"

    else

        # Strip the './dist/' prefix from $FILE to get the relative path
        directory_path="$TARGET_DIR${FILE#dist/}"

        # Extract the directory part of the relative path
        DIR_PATH="$(dirname "$directory_path")/"

        printf "\n ${YELLOW}⌛ Uploading regular file: $FILE${NC} to 🐈 $directory_path \n\n"

        RESPONSE=$(curl --request POST \
            --url https://nekoweb.org/api/files/upload \
            --header "Authorization: $API_KEY" \
            --form pathname=$DIR_PATH \
            --form files=@$FILE \
            --include)

        HEADER=$(echo "$RESPONSE" | awk 'BEGIN{found=0} /^$/{found=1; next} !found')
        BODY=$(echo "$RESPONSE" | awk 'BEGIN{found=0} /^$/{found=1; next} found')

        if [[ "$BODY" != "No files" && "$BODY" != "Files uploaded" ]]; then
            echo $HEADER
            echo $BODY
            read -t 30 -p "Hmmmmm, an error occurred. Do you want to continue? [y/N] " CONTINUE

            # Check if CONTINUE is empty (i.e., user did not input within 30 seconds)
            if [[ -z "$CONTINUE" ]]; then
                CONTINUE="y" # Default to 'y' if no input is received
            fi

            if [[ "$CONTINUE" == "y" || "$CONTINUE" == "Y" ]]; then
                printf "\n${GREEN}Continuing..."
            elif [[ "$CONTINUE" == "n" || "$CONTINUE" == "N" ]]; then
                printf "${RED} exiting script."
                exit 1
            else
                echo "Invalid option. Skipping to the next file."
                continue
            fi
        else

            RATELIMIT_LIMIT=$(echo "$HEADER" | awk -F': ' '/^ratelimit-limit:/ {print $2}')
            RATELIMIT_REMAINING=$(echo "$HEADER" | awk -F': ' '/^ratelimit-remaining:/ {print $2}')
            RATELIMIT_RESET=$(echo "$HEADER" | awk -F': ' '/^ratelimit-reset:/ {print $2}')

            # Determine if we need to wait based on the 'ratelimit-remaining' value
            if [ "$RATELIMIT_REMAINING" -le 1 ]; then
                echo -e "Rate limit is low. Waiting ${RATELIMIT_RESET} seconds before making another request."
                sleep "$RATELIMIT_RESET"
            # else
            #     echo -e "You should wait ${RATELIMIT_RESET} seconds before making another request."
            fi

            # Define thresholds based on 'ratelimit-limit'
            # For example, HIGH_THRESHOLD is 50% of ratelimit-limit, MEDIUM_THRESHOLD is 20%
            HIGH_THRESHOLD=$(echo "$RATELIMIT_LIMIT * 0.5" | bc)
            MEDIUM_THRESHOLD=$(echo "$RATELIMIT_LIMIT * 0.2" | bc)

            # Determine the color based on the rate limit remaining
            if (($(echo "$RATELIMIT_REMAINING >= $HIGH_THRESHOLD" | bc -l))); then
                COLOR=$GREEN
            elif (($(echo "$RATELIMIT_REMAINING >= $MEDIUM_THRESHOLD" | bc -l))); then
                COLOR=$YELLOW
            else
                COLOR=$RED
            fi
            printf "\n ${COLOR} Rate limit remaining: ${RATELIMIT_REMAINING}${NC} \n"

            if [[ $BODY == "No files" ]]; then

                printf "\n${RED}   (x_x):  $FILE${NC}\n"

            elif [[ $BODY == "Files uploaded" ]]; then
                printf "\n${GREEN}    (´,,•ω•,,)♡    uploaded $FILE${NC} to $directory_path ${NC}\n"
            fi
        fi

        printf "\n${BLUE}▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰${NC}\n"
    fi
done

printf "${GREEN} 	(ﾉ◕ヮ◕)ﾉ*:･ﾟ✧ 	All files uploaded successfully.${NC}"
